﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.Models;

namespace UniResearch.Data
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            this.Comments = new HashSet<Comment>();
            this.UsersPapers = new HashSet<UserPaper>();
        }
        [Required]
        [Display(Name ="Profile name")]
        public string ProfileName { get; set; }
        [Required]
        [Display(Name = "Degree")]
        public string Degree { get; set; }
        [Required]
        [Display(Name = "University")]
        public string University { get; set; }
        [Required]
        [Display(Name = "Faculty")]
        public string Faculty { get; set; }

        [Display(Name = "Profile photo")]
        public string ImagePath { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<UserPaper> UsersPapers { get; set; }
    }
}
