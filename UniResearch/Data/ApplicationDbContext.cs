﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using UniResearch.Models;

namespace UniResearch.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Paper> Papers { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<UserPaper> UsersPapers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<ApplicationUser>()
            //    .HasKey(x => x.Id);

            modelBuilder.Entity<Paper>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Comment>()
                .HasKey(x => x.Id);


            // configures one-to-one relationship
            //modelBuilder.Entity<Author>()
            //    .HasOne(a => a.Biography)
            //    .WithOne(b => b.Author)
            //    .HasForeignKey<AuthorBiography>(b => b.AuthorRef);

            // configures one-to-many relationship
            modelBuilder.Entity<Paper>()
                .HasMany(c => c.Comments)
                .WithOne(e => e.Paper);

            //configures many-to-many relationship
            modelBuilder.Entity<UserPaper>()
                .HasKey(x => new { x.ApplicationUserId, x.PaperId });

            modelBuilder.Entity<UserPaper>()
                .HasOne(x => x.ApplicationUser)
                .WithMany(m => m.UsersPapers)
                .HasForeignKey(x => x.ApplicationUserId);

            modelBuilder.Entity<UserPaper>()
                .HasOne(x => x.Paper)
                .WithMany(e => e.UsersPapers)
                .HasForeignKey(x => x.PaperId);

        }
    }
}
