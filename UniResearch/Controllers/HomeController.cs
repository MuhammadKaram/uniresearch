﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UniResearch.Models;
using UniResearch.ViewModels;

namespace UniResearch.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISearchRepository _searchRepository;

        public HomeController(ISearchRepository searchRepository)
        {
            _searchRepository = searchRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            SearchViewModel searchViewModel = new SearchViewModel();
            return View(searchViewModel);
        }

        [HttpPost]
        public IActionResult Index(SearchViewModel model)
        {
            return RedirectToAction("SearchByPaperName", "Search", new { searchKey = model.SearchKey });
            //return RedirectToAction("SearchByPaperName", "Search", new { searchViewModel = model });
            //return RedirectToAction("SearchByPaperName", new { searchViewModel = model });
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Mariam()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
