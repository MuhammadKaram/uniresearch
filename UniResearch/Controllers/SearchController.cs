﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UniResearch.Models;
using UniResearch.ViewModels;

namespace UniResearch.Controllers
{
    public class SearchController : Controller
    {
        private readonly ISearchRepository _searchRepository;

        public SearchController(ISearchRepository searchRepository)
        {
            _searchRepository = searchRepository;
        }

        [HttpGet]
        public IActionResult SearchByPaperName(string searchKey)
        {
            SearchViewModel result = _searchRepository.SearchByPaperName(searchKey);
            return View("PaperSearch", result);
        }

        [HttpPost]
        public IActionResult SearchByPaperName(SearchViewModel searchViewModel)
        {
            return RedirectToAction("SearchByPaperName", new { searchKey = searchViewModel.SearchKey });
            //SearchViewModel result = _searchRepository.SearchByPaperName(searchViewModel.SearchKey);
            //return View("PaperSearch", result);
        }
    }
}