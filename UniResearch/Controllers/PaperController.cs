﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniResearch.Models;
using UniResearch.ViewModels;

namespace UniResearch.Controllers
{
    public class PaperController : Controller
    {
        private readonly IPaperRepository _paperRepository;
        private readonly IHostingEnvironment _hostingEnvironment;

        public PaperController(IPaperRepository paperRepository, IHostingEnvironment hostingEnvironment)
        {
            _paperRepository = paperRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public IActionResult AddPaper(string userId)
        {
            AddPaperViewModel model = new AddPaperViewModel { ProfileUserId = userId };
            return View(model);
        }

        [HttpPost]
        public IActionResult AddPaper(AddPaperViewModel model)
        {
            //if (ModelState.IsValid)
            //{
            //if (model.Paper == null || model.PaperName == null)
            //    return RedirectToAction("Profile", "Profile", new { userId = model.ProfileUserId });
            Paper paper = new Paper();
            paper.PaperName = model.PaperName;
            paper.PaperFilePath = FileUpload(model.Paper);
            paper.UsersPapers.Add(new UserPaper { ApplicationUserId = model.ProfileUserId, PaperId = model.Id });
            _paperRepository.AddPaper(paper);
            //}
            return RedirectToAction("Profile", "Profile", new { userId = model.ProfileUserId });
        }

        private string FileUpload(IFormFile File)
        {
            string uniqueFileName = null;

            // If the Photo property on the incoming model object is not null, then the user
            // has selected an image to upload.
            if (File != null)
            {
                // The image must be uploaded to the images folder in wwwroot
                // To get the path of the wwwroot folder we are using the inject
                // HostingEnvironment service provided by ASP.NET Core
                string uploadsFolder = Path.Combine(_hostingEnvironment.WebRootPath, "papers");
                // To make sure the file name is unique we are appending a new
                // GUID value and and an underscore to the file name
                uniqueFileName = Guid.NewGuid().ToString() + "_" + File.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                // Use CopyTo() method provided by IFormFile interface to
                // copy the file to wwwroot/images folder
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    File.CopyTo(fileStream);
                }
                //File.CopyTo(new FileStream(filePath, FileMode.Create));
            }
            return uniqueFileName;
        }

        public async Task<IActionResult> Download(string filename)
        {
            //if (filename == null)
            //    return Content("filename not present");

            var path = Path.Combine(_hostingEnvironment.WebRootPath, "papers", filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, GetContentType(path), Path.GetFileName(path));
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats" +
                "officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }

        public IActionResult Delete(int paperId)
        {
            var paperFilePath = _paperRepository.GetPaper(paperId).PaperFilePath;
            _paperRepository.DeletePaper(paperId, User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);
            //if (paperFilePath != null)
            //{
            //    string filePath = Path.Combine(_hostingEnvironment.WebRootPath, "papers", paperFilePath);
            //    System.IO.File.Delete(filePath);
            //}
            return RedirectToAction("Profile", "Profile", new { userId = User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value });
        }
    }
}