﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UniResearch.Models;
using UniResearch.ViewModels;

namespace UniResearch.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IPaperRepository _paperRepository;
        private readonly IProfileRepository _profileRepository;

        public CommentController(ICommentRepository commentRepository, IPaperRepository paperRepository, IProfileRepository profileRepository)
        {
            _commentRepository = commentRepository;
            _paperRepository = paperRepository;
            _profileRepository = profileRepository;
        }

        [HttpGet]
        public IActionResult PaperComments(int paperId)
        {
            var comments = _commentRepository.GetPaperComments(paperId);
            comments = comments.Reverse();
            var paper = _paperRepository.GetPaper(paperId);
            string loggedProfileName;
            CommentViewModel model = new CommentViewModel();
            try
            {
                loggedProfileName = _profileRepository.GetProfileName(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

                model.ApplicationUserId = User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value;
                model.CommentProfileName = loggedProfileName;
                model.Comments = comments;
                model.Paper = paper;
            }
            catch (Exception ex)
            {
                loggedProfileName = null;

                model.ApplicationUserId = null;
                model.CommentProfileName = loggedProfileName;
                model.Comments = comments;
                model.Paper = paper;
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult PaperComments(CommentViewModel commentViewModel)
        {
            //if (ModelState.IsValid)
            //{
            if (commentViewModel.CommentText == null)
                return RedirectToAction("PaperComments", new { paperId = commentViewModel.Paper.Id });

            Comment comment = new Comment
            {
                PaperId = commentViewModel.Paper.Id,
                ApplicationUserId = commentViewModel.ApplicationUserId,
                CommentText = commentViewModel.CommentText
            };
            _commentRepository.AddComment(comment);
            //}
            return RedirectToAction("PaperComments", new { paperId = commentViewModel.Paper.Id });
        }

        public IActionResult DeleteComment(int commentId, int papId)
        {
            _commentRepository.DeleteComment(commentId);
            return RedirectToAction("PaperComments", new { paperId = papId });
        }
    }
}