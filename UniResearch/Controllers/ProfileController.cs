﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UniResearch.Models;
using UniResearch.ViewModels;

namespace UniResearch.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IProfileRepository _profileRepository;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProfileController(IProfileRepository profileRepository, IHostingEnvironment hostingEnvironment)
        {
            _profileRepository = profileRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public IActionResult Profile(string userId)
        {
            ProfileViewModel profile = _profileRepository.GetProfile(userId);
            try
            {
                profile.LoggedUser = (userId == User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value ? true : false);
            }
            catch (Exception ex)
            {
                profile.LoggedUser = false;
            }
            return View(profile);
        }

        [HttpGet]
        public IActionResult UpdateProfile(string userId)
        {
            ProfileViewModel profile = _profileRepository.GetProfile(userId);
            ProfileUpdateViewModel profileUpdateViewModel = new ProfileUpdateViewModel
            {
                UserId = profile.ApplicationUser.Id,
                ProfileName = profile.ApplicationUser.ProfileName,
                University = profile.ApplicationUser.University,
                Faculty = profile.ApplicationUser.Faculty,
                Degree = profile.ApplicationUser.Degree,
                ExcistingImagePath = profile.ApplicationUser.ImagePath
            };
            return View(profileUpdateViewModel);
        }

        [HttpPost]
        public IActionResult UpdateProfile(ProfileUpdateViewModel profileUpdateViewModel)
        {
            if (ModelState.IsValid)
            {
                ProfileViewModel profile = _profileRepository.GetProfile(profileUpdateViewModel.UserId);
                profile.ApplicationUser.ProfileName = profileUpdateViewModel.ProfileName;
                profile.ApplicationUser.University = profileUpdateViewModel.University;
                profile.ApplicationUser.Faculty = profileUpdateViewModel.Faculty;
                profile.ApplicationUser.Degree = profileUpdateViewModel.Degree;

                if (profileUpdateViewModel.Image != null)
                {
                    if (profileUpdateViewModel.ExcistingImagePath != null)
                    {
                        string filePath = Path.Combine(_hostingEnvironment.WebRootPath, "images", profileUpdateViewModel.ExcistingImagePath);
                        System.IO.File.Delete(filePath);
                    }
                    profile.ApplicationUser.ImagePath = ImageUpload(profileUpdateViewModel.Image);
                }

                _profileRepository.UpdateProfile(profile);
            }

            return RedirectToAction("Profile", new { userId = profileUpdateViewModel.UserId });
        }

        private string ImageUpload(IFormFile ImagePath)
        {
            string uniqueFileName = null;

            // If the Photo property on the incoming model object is not null, then the user
            // has selected an image to upload.
            if (ImagePath != null)
            {
                // The image must be uploaded to the images folder in wwwroot
                // To get the path of the wwwroot folder we are using the inject
                // HostingEnvironment service provided by ASP.NET Core
                string uploadsFolder = Path.Combine(_hostingEnvironment.WebRootPath, "images");
                // To make sure the file name is unique we are appending a new
                // GUID value and and an underscore to the file name
                uniqueFileName = Guid.NewGuid().ToString() + "_" + ImagePath.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                // Use CopyTo() method provided by IFormFile interface to
                // copy the file to wwwroot/images folder
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    ImagePath.CopyTo(fileStream);
                }
                //ImagePath.CopyTo(new FileStream(filePath, FileMode.Create));
            }
            return uniqueFileName;
        }
    }
}