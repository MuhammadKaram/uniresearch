﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.Data;
using UniResearch.Models;

namespace UniResearch.ViewModels
{
    public class ProfileViewModel
    {
        public ApplicationUser ApplicationUser { get; set; }
        public IEnumerable<Paper> Paper { get; set; }
        public bool LoggedUser { get; set; }
    }
}
