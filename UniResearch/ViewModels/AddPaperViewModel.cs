﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.Data;
using UniResearch.Models;

namespace UniResearch.ViewModels
{
    public class AddPaperViewModel : Paper
    {
        [Required]
        public string ProfileUserId { get; set; }
        public IEnumerable<ApplicationUser> Profiles { get; set; }
        [Required(ErrorMessage = "Please upload your paper file.")]
        [Display(Name = "Paper File")]
        public IFormFile Paper { get; set; }
    }
}
