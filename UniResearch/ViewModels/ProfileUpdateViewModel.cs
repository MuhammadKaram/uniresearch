﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UniResearch.ViewModels
{
    public class ProfileUpdateViewModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "Profile Name")]
        public string ProfileName { get; set; }

        [Required]
        [Display(Name = "Degree")]
        public string Degree { get; set; }

        [Required]
        [Display(Name = "University")]
        public string University { get; set; }

        [Required]
        [Display(Name = "Faculty")]
        public string Faculty { get; set; }

        //[Required]
        [Display(Name = "Excisting profile image")]
        public string ExcistingImagePath { get; set; }

        //[Required]
        [Display(Name = "Profile image")]
        public IFormFile Image { get; set; }
    }
}
