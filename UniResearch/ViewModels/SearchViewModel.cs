﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.Models;

namespace UniResearch.ViewModels
{
    public class SearchViewModel
    {
        public string SearchKey { get; set; }
        public IEnumerable<Paper> Result { get; set; }
    }
}
