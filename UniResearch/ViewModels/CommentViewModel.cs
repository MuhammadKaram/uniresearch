﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.Models;

namespace UniResearch.ViewModels
{
    public class CommentViewModel
    {
        public Paper Paper { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
        public string ApplicationUserId { get; set; }
        public string CommentProfileName { get; set; }
        [Required]
        public string CommentText { get; set; }
    }
}
