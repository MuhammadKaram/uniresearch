﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.Data;
using UniResearch.ViewModels;

namespace UniResearch.Models
{
    public class SearchRepository : ISearchRepository
    {
        private readonly ApplicationDbContext context;

        public SearchRepository(ApplicationDbContext context)
        {
            this.context = context;
        }
        public SearchViewModel SearchByPaperName(string paperName)
        {
            IEnumerable<Paper> papers = context.Papers.Where(x => x.PaperName.Contains(paperName));
            papers = papers.Reverse();
            foreach(var paper in papers)
            {
                paper.UsersPapers = context.Entry(paper).Collection(u => u.UsersPapers).Query().ToList();
            }
            SearchViewModel result = new SearchViewModel
            {
                SearchKey = paperName,
                Result = papers
            };
            return result;
        }
    }
}
