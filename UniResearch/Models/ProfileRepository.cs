﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.Data;
using UniResearch.ViewModels;

namespace UniResearch.Models
{
    public class ProfileRepository : IProfileRepository
    {
        private readonly ApplicationDbContext context;

        public ProfileRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public ProfileViewModel GetProfile(string userId)
        {
            ApplicationUser applicationUser = context.ApplicationUsers.FirstOrDefault(u => u.Id == userId);
            applicationUser.UsersPapers = context.Entry(applicationUser).Collection(u => u.UsersPapers).Query().ToList();
            var userPapers = context.Papers.Where(x => x.UsersPapers.Any(c => c.ApplicationUserId == userId));

            foreach(var paper in userPapers)
            {
                paper.UsersPapers = context.Entry(paper).Collection(u => u.UsersPapers).Query().ToList();
            }

            ProfileViewModel model = new ProfileViewModel
            {
                ApplicationUser = applicationUser,
                Paper = userPapers
            };
            return model;
        }

        public string GetProfileName(string userId)
        {
            return context.ApplicationUsers.FirstOrDefault(x => x.Id == userId).ProfileName;
        }

        public ProfileViewModel UpdateProfile(ProfileViewModel profileViewModel)
        {
            var profile = context.ApplicationUsers.Attach(profileViewModel.ApplicationUser);
            profile.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
            return profileViewModel;
        }
    }
}
