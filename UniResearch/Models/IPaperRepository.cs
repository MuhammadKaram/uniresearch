﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UniResearch.Models
{
    public interface IPaperRepository
    {
        IEnumerable<Paper> GetAllPapers();
        IEnumerable<Paper> GetProfilePapers(string userId);
        Paper GetPaper(int id);
        Paper AddPaper(Paper paper);
        Paper DeletePaper(int id, string loggedUserId);
    }
}
