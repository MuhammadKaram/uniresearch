﻿using System;
using System.Collections.Generic;

namespace UniResearch.Models
{
    public partial class Profile
    {
        public Profile()
        {
            this.ProfilesPapers = new HashSet<UserPaper>();
        }

        public int Id { get; set; }
        public string UserId { get; set; }
        public string ProfileName { get; set; }
        public string Degree { get; set; }
        public string University { get; set; }
        public string Faculty { get; set; }
        public string ProfileImagePath { get; set; }

        public virtual ICollection<UserPaper> ProfilesPapers { get; set; }
    }
}
