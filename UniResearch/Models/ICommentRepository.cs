﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UniResearch.Models
{
    public interface ICommentRepository
    {
        IEnumerable<Comment> GetAllComments();
        IEnumerable<Comment> GetProfileComments(string userId);
        IEnumerable<Comment> GetPaperComments(int paperId);
        Comment GetComment(int id);
        Comment AddComment(Comment comment);
        Comment DeleteComment(int id);
    }
}
