﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UniResearch.Models
{
    public partial class Paper
    {
        public Paper()
        {
            this.Comments = new HashSet<Comment>();
            this.UsersPapers = new HashSet<UserPaper>();
        }

        [Required]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Paper name")]
        public string PaperName { get; set; }
        [Required]
        [Display(Name = "Paper file")]
        public string PaperFilePath { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<UserPaper> UsersPapers { get; set; }
    }
}
