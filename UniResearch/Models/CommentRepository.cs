﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.Data;

namespace UniResearch.Models
{
    public class CommentRepository : ICommentRepository
    {
        private readonly ApplicationDbContext context;

        public CommentRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public Comment AddComment(Comment comment)
        {
            context.Comments.Add(comment);
            context.SaveChanges();
            return comment;
        }

        public Comment DeleteComment(int id)
        {
            Comment comment = context.Comments.Find(id);
            context.Comments.Remove(comment);
            context.SaveChanges();
            return comment;
        }

        public IEnumerable<Comment> GetAllComments()
        {
            return context.Comments;
        }

        public Comment GetComment(int id)
        {
            return context.Comments.Find(id);
        }

        public IEnumerable<Comment> GetPaperComments(int paperId)
        {
            // Include => to fill the related data
            var comments = context.Comments.Include(comment => comment.ApplicationUser).Where(x => x.PaperId == paperId);
            return comments; // context.Comments.Where(x => x.PaperId == paperId);
        }

        public IEnumerable<Comment> GetProfileComments(string userId)
        {
            return context.Comments.Where(x => x.ApplicationUserId == userId);
        }
    }
}
