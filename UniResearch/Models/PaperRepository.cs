﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.Data;

namespace UniResearch.Models
{
    public class PaperRepository : IPaperRepository
    {
        private readonly ApplicationDbContext context;
        private readonly IHostingEnvironment hostingEnvironment;

        public PaperRepository(ApplicationDbContext context, IHostingEnvironment hostingEnvironment)
        {
            this.context = context;
            this.hostingEnvironment = hostingEnvironment;
        }
        public Paper AddPaper(Paper paper)
        {
            context.Papers.Add(paper);
            context.SaveChanges();
            return paper;
        }

        public Paper DeletePaper(int id, string loggedUserId)
        {
            Paper paper = context.Papers.Find(id);
            var userPapersCount = context.UsersPapers.Where(x => x.PaperId == id).Count();
            if (userPapersCount > 1)
            {
                UserPaper loggedUserPaper = context.UsersPapers.FirstOrDefault(x => x.ApplicationUserId == loggedUserId && x.PaperId == id);
                context.UsersPapers.Remove(loggedUserPaper);
                context.SaveChanges();
            }
            if (userPapersCount == 1)
            {
                if (paper != null)
                {
                    context.Papers.Remove(paper);
                    context.SaveChanges();
                    if (paper.PaperFilePath != null)
                    {
                        string filePath = System.IO.Path.Combine(hostingEnvironment.WebRootPath, "papers", paper.PaperFilePath);
                        System.IO.File.Delete(filePath);
                    }
                }
            }
            return paper;
        }

        public IEnumerable<Paper> GetAllPapers()
        {
            return context.Papers;
        }

        public Paper GetPaper(int id)
        {
            var paper = context.Papers.Find(id);
            paper.UsersPapers = context.Entry(paper).Collection(u => u.UsersPapers).Query().ToList();
            return paper;
        }

        public IEnumerable<Paper> GetProfilePapers(string userId)
        {
            return context.Papers.Where(x => x.UsersPapers.Any(c => c.ApplicationUserId == userId));
        }
    }
}
