﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.ViewModels;

namespace UniResearch.Models
{
    public interface IProfileRepository
    {
        ProfileViewModel GetProfile(string userId);
        ProfileViewModel UpdateProfile(ProfileViewModel profileViewModel);
        string GetProfileName(string userId);
    }
}
