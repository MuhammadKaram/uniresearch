﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UniResearch.Data;

namespace UniResearch.Models
{
    public partial class Comment
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int PaperId { get; set; }
        [Required]
        public string ApplicationUserId { get; set; }
        [Required]
        [Display(Name = "Comment")]
        public string CommentText { get; set; }

        public virtual Paper Paper { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
