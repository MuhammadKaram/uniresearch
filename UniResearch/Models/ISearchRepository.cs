﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniResearch.ViewModels;

namespace UniResearch.Models
{
    public interface ISearchRepository
    {
        SearchViewModel SearchByPaperName(string paperName);
    }
}
