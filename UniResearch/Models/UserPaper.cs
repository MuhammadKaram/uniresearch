﻿using System;
using System.Collections.Generic;
using UniResearch.Data;

namespace UniResearch.Models
{
    public partial class UserPaper
    {
        public string ApplicationUserId { get; set; }
        public int PaperId { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Paper Paper { get; set; }
    }
}
